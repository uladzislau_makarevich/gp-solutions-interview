/**
 * ЗАДАЧА №278
 * <p>
 * Вычислительная биология
 * (Время: 2 сек. Память: 16 Мб Сложность: 28%)
 * <p>
 * В современной биологии ученым часто приходится иметь дело с последовательностями ДНК. Эти последовательности зачастую являются очень длинными, и их ручная обработка требует большого количества времени и сил. Поэтому возникает идея автоматизировать этот процесс.
 * <p>
 * Для этого можно применять компьютерные методы обработки данных, например, весьма полезными оказываются алгоритмы на строках. В этой задаче последовательность ДНК будет представляться в виде непустой строки, все символы которой входят в множество {A, G, С, T}.
 * <p>
 * Пусть даны две последовательности ДНК: s = s1s2 … sn и t = t1t2 … tm. Будем говорить, что t может получится в результате эволюции из s, если s является подпоследовательностью t, то есть существует такая последовательность индексов 1 ≤ i1 < i2 < … < in ≤ m, что s1=ti1, s2=ti2, … sn=tin. Необходимо выяснить, может ли последовательность t получится в результате эволюции из s.
 * Входные данные
 * <p>
 * Первая строка входного файла INPUT.TXT содержит последовательность s, вторая — последовательность t. Размер входного файла не превосходит 256 килобайт.
 * Выходные данные
 * <p>
 * В выходной файл OUTPUT.TXT выведите слово YES, если последовательность t могла получиться в результате эволюции из s, и слово NO — иначе.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Biology {

    static void check() throws IOException {

        File file = new File("INPUT.TXT");
        Scanner scanner = new Scanner(file);
        char[] input = scanner.next().toCharArray();
        char[] result = scanner.next().toCharArray();

        int j = 0;

        for (char item : result) {
            if (item == input[j]) {
                j++;
            }
        }

        File output = new File("OUTPUT.TXT");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(output));

        if (j == input.length) {
            bufferedWriter.write("YES");
        } else {
            bufferedWriter.write("NO");
        }

        bufferedWriter.close();
    }

    public static void main(String[] args) throws IOException {
        Biology.check();
    }
}
