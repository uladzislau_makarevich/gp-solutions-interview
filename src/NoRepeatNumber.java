import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * ЗАДАЧА №670
 * <p>
 * Числа без одинаковых цифр
 * (Время: 1 сек. Память: 16 Мб Сложность: 25%)
 * <p>
 * Антон записал ряд натуральных чисел в порядке возрастания: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 и т.д. Затем вычеркнул из него все числа, в которых имеется хотя бы две одинаковых цифры, и получил последовательность: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23 и т.д.
 * <p>
 * Вам необходимо по заданному N найти N-ое по счету число в получившейся последовательности.
 * Входные данные
 * <p>
 * В единственной строке входного файла INPUT.TXT записано натуральное число N (1 ≤ N ≤ 10000).
 * Выходные данные
 * <p>
 * В единственную строку выходного файла OUTPUT.TXT нужно вывести N-ое по счету число без одинаковых цифр.
 */

public class NoRepeatNumber {

    static void sayNumber() throws IOException {

        File file = new File("INPUT.TXT");
        Scanner scanner = new Scanner(file);
        int numberByOrder = scanner.nextInt();
        scanner.close();

        int i = 0, last = 1;
        while (i <= 10000 && numberByOrder > 0) {
            i++;
            char[] temp = Integer.toString(i).toCharArray();
            Arrays.sort(temp);

            numberByOrder--;
            for (int j = 0; j < temp.length - 1; j++) {
                if (temp[j] == temp[j + 1]) {
                    numberByOrder++;
                    break;
                }
            }
            last = i;

        }

            File output = new File("OUTPUT.TXT");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(output));
        bufferedWriter.write("" + last);
            bufferedWriter.close();
    }

    public static void main(String[] args) throws IOException {
        NoRepeatNumber.sayNumber();
    }
}
