import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * ЗАДАЧА №557
 * <p>
 * Матрицы
 * (Время: 2 сек. Память: 16 Мб Сложность: 42%)
 * <p>
 * Аня недавно узнала, что такое квадратная матрица размерности n. Это таблица n×n с целыми числами в ячейках. Число, стоящее на пересечении i-ой строки и j-ого столбца матрицы A, кратко обозначается A[i, j]. Матрицы можно умножать, и Аня быстро освоила, как запрограммировать эту операцию с помощью циклов. Результатом умножения двух матриц A и B будет матрица C, элементы которой определяются следующим образом:
 * <p>
 * Матрицы ей понадобились для конкретной задачи, в которой надо узнать определенный элемент произведения нескольких матриц. Это уже достаточно сложная задача для Ани, но она усложняется тем, что все вычисления ведутся по модулю некоторого простого числа p, то есть если при арифметических операциях получается число, большее, либо равное p, оно заменяется на остаток при делении на p.
 * <p>
 * Помогите Ане вычислить нужный ей элемент.
 * Входные данные
 * <p>
 * В первой строчке входного файла INPUT.TXT стоят два числа: m - количество матриц, n - размер каждой из матриц (1 ≤ m ≤ 130, 1 ≤ n ≤ 130). В следующей строчке содержатся номер строки и столбца, интересующего Аню элемента 1 ≤ a ≤ n, 1 ≤ b ≤ n. В третьей строке содержится простое число p ≤ 1000. Далее следует описание m матриц. Описание каждой матрицы состоит из n строк. В каждой из строк содержится n неотрицательных целых чисел, меньших p. Соседние числа в строке разделены пробелом, а перед каждой матрицей пропущена строка.
 * Выходные данные
 * <p>
 * В выходной файл OUTPUT.TXT выведите нужный Ане элемент произведения матриц.
 */

public class Matrices {

    static void calc() throws IOException {

        File file = new File("INPUT.TXT");
        Scanner scanner = new Scanner(file);

        int matrices = scanner.nextInt();
        int size = scanner.nextInt();

        int i0 = scanner.nextInt() - 1;
        int j0 = scanner.nextInt() - 1;

        int p = scanner.nextInt();

        int[][] first = new int[0][], second, temp;


        for (int m = 0; m < matrices; m++) {
            temp = new int[size][size];
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    temp[i][j] = scanner.nextInt();
                }
            }
            if (m == 0) {
                first = temp;
                continue;
            }
            second = temp;

            temp = new int[size][size];

            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    for (int k = 0; k < size; k++) {
//                        System.out.print(temp[i][j] + " ");
                        temp[i][j] += first[i][k] * second[k][j];
//                        System.out.println(temp[i][j] + "(" + first[i][k] + ", " + second[k][j] + ")");
                    }
                }
            }

//            System.out.println(Arrays.deepToString(first));

            first = temp;

        }

        scanner.close();

        File output = new File("OUTPUT.TXT");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(output));

        bufferedWriter.write("" + first[i0][j0] % p);

        bufferedWriter.close();
    }

    public static void main(String[] args) throws IOException {
        Matrices.calc();
    }

}
