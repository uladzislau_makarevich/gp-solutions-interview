import java.io.*;

/**
 * Напёрстки
 (Время: 1 сек. Память: 16 Мб Сложность: 15%)

 Шулер показывает следующий трюк. Он имеет три одинаковых наперстка. Под первый (левый) он кладет маленький шарик. Затем он очень быстро выполняет ряд перемещений наперстков, каждое из которых – это одно из трех перемещений - A, B, C:

 A - обменять местами левый и центральный наперстки,
 B - обменять местами правый и центральный наперстки,
 C - обменять местами левый и правый наперстки.

 Необходимо определить, под каким из наперстков окажется шарик после всех перемещений.
 Входные данные

 В единственной строке входного файла INPUT.TXT записана строка длиной от 1 до 50 символов из множества {A, B, C} – последовательность перемещений.
 Выходные данные

 В единственную строку выходного файла OUTPUT.TXT нужно вывести номер наперстка, под которым окажется шарик после перемещений.
 */

public class Thimbles {

    public static int whereIsMyBall(String input) {

        int position = 0;

        for(Character c : input.toCharArray()) {
            switch (c) {
                case 'A': {
                    position = (position == 0) ? 1 : (position == 1) ? 0 : position;
                    break;
                }
                case 'B': {
                    position = (position == 2) ? 1 : (position == 1) ? 2 : position;
                    break;
                }
                case 'C': {
                    position = (position == 0) ? 2 : (position == 2) ? 0 : position;
                    break;
                }
            }
        }

        return position;
    }

    public static void whereIsMyBall(File test) {
        try {
            FileReader fileReader = new FileReader(test);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String input = bufferedReader.readLine();
            bufferedReader.close();

            PrintWriter writer = new PrintWriter("OUTPUT.TXT", "UTF-8");
            writer.print(whereIsMyBall(input));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
