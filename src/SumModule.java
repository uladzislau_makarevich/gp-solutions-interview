import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Модуль суммы
 * (Время: 1 сек. Память: 16 Мб Сложность: 25%)
 * <p>
 * Дана последовательность целых чисел. Требуется найти подпоследовательность заданной последовательности с максимальным модулем суммы входящих в нее чисел. Напомним, что модуль целого числа x равняется x, если x ≥ 0 и -x, если x < 0.
 * Входные данные
 * <p>
 * Первая строка входного файла INPUT.TXT содержит натуральное число n (1 ≤ n ≤ 10000) - длину последовательности. Во второй строке записаны n целых чисел, по модулю не превосходящих 10000.
 * Выходные данные
 * <p>
 * В первой строке выходного файла OUTPUT.TXT выведите длину k выбранной вами подпоследовательности. Во второй строке должны быть записаны k различных чисел, разделенных пробелами - номера выбранных членов последовательности.
 */

class SumModule {

    static void findAll() throws IOException {

        File file = new File("INPUT.TXT");
        Scanner scanner = new Scanner(file);
        int amount = scanner.nextInt();
        int[] positions = new int[amount];
        int sum_pos = 0, sum_neg = 0, temp;
        int j = 0, k = amount;

        for (int i = 0; i < amount; i++) {
            if ((temp = scanner.nextInt()) >= 0) {
                sum_pos += temp;
                positions[j++] = i;
            } else {
                sum_neg += Math.abs(temp);
                positions[--k] = i;
            }
        }

        File output = new File("OUTPUT.TXT");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(output));

        StringBuilder result = new StringBuilder();

        if (sum_pos >= sum_neg) {
            for (int i = 0; i < j; i++) {
                result.append(positions[i] + 1).append(" ");
            }
            temp = j;
        } else {
            for (int i = amount - 1; i >= k; i--) {
                result.append(positions[i] + 1).append(" ");
            }
            temp = amount - k;
        }

        result.deleteCharAt(result.length() - 1);

        bufferedWriter.write("" + temp);
        bufferedWriter.newLine();
        bufferedWriter.write(result.toString());

        bufferedWriter.close();
    }

    public static void main(String[] args) throws IOException {
        SumModule.findAll();
    }
}
