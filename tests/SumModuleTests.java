import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class SumModuleTests {

    @Test
    public void standardTest() {
        try {
            File file = new File("INPUT.TXT");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write("5\n" +
                    "-1 4 -1 6 -7");
            bufferedWriter.close();

            SumModule.findAll();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("OUTPUT.TXT")));

            Assert.assertEquals("2", bufferedReader.readLine());
            Assert.assertEquals("2 4", bufferedReader.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
