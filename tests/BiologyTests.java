import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class BiologyTests {

    @Test
    public void standardTest1() {
        try {
            File file = new File("INPUT.TXT");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write("GTA\n" +
                    "AGCTA");
            bufferedWriter.close();

            Biology.check();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("OUTPUT.TXT")));

            Assert.assertEquals("YES", bufferedReader.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void standardTest2() {
        try {
            File file = new File("INPUT.TXT");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write("AAAG\n" +
                    "GAAAAAT");
            bufferedWriter.close();

            Biology.check();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("OUTPUT.TXT")));

            Assert.assertEquals("NO", bufferedReader.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
