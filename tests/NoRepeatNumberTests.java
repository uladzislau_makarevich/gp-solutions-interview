import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class NoRepeatNumberTests {

    @Test
    public void standardTest() {
        try {
            File file = new File("INPUT.TXT");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write("100");
            bufferedWriter.close();

            NoRepeatNumber.sayNumber();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("OUTPUT.TXT")));

            Assert.assertEquals("123", bufferedReader.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
