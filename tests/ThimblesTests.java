import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ThimblesTests {

    @Test
    public void basicTest() {
        Assert.assertEquals(0, Thimbles.whereIsMyBall("CBABCACCC"));
    }

    @Test
    public void fileTest() {
        int result = -1;
        try {
            File file = File.createTempFile("INPUT", ".TXT");
            FileWriter writer = new FileWriter(file);
            writer.append("CBABCACCC");
            writer.close();
            Thimbles.whereIsMyBall(file);
            result = new FileReader("OUTPUT.TXT").read() - 48;
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(0, result);

    }
}
