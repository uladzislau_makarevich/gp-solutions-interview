import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class MatricesTests {

    @Test
    public void standardTest() {
        try {
            File file = new File("INPUT.TXT");
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            bufferedWriter.write(
                    "3 2\n" +
                            "1 2\n" +
                            "239\n" +
                            "\n" +
                            "1 2\n" +
                            "3 4\n" +
                            "\n" +
                            "4 2\n" +
                            "1 3\n" +
                            "\n" +
                            "1 2\n" +
                            "2 1 ");
            bufferedWriter.close();

            Matrices.calc();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("OUTPUT.TXT")));

            Assert.assertEquals("20", bufferedReader.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
